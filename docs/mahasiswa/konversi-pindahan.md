---
sidebar position: 5
---

# Konversi Pindahan (PMB)

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Konversi Pindahan (PMB)** ini berguna bagi kita dalam mengubah, menambah, dan melihat data Konversi Mahasiswa Pindahan (PMB), berikut cara menggunakannya:

1. Menambah Data Konversi Pindahan (PMB).
2. Mengubah Data Konveri Pindahan (PMB).
3. Menghapus Data Konveri Pindahan (PMB).
4. Melihat Data Konveri Pindahan (PMB).

# Menambah Data Konversi Pindahan (PMB)

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa** > **Konversi Pindahan (PMB)**.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-pindahan(PMB)/konversi-pindahan.png)

2. Klik icon berikut pada bagian tabel aksi.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-pindahan(PMB)/tambah-konversi-pindahan.png)

3. Isi **form** pada halaman tersebut

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-pindahan(PMB)/isi-from-tambah.png)

4. Jika telah selesai, klik **Simpan**.

# Mengubah Data Konversi Pindahan (PMB)

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa > **Konversi Pindahan (PMB)**.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-pindahan(PMB)/konversi-pindahan.png)

2. Klik ikon mata pada bagian tabel aksi.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-pindahan(PMB)/edit-konversi-pindahan.png)

3. Isi **form** pada halaman tersebut


![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-pindahan(PMB)/edit-isi-form.png)

4. Jika telah selesai, klik **Simpan**

# Menghapus Data Konversi Pindahan (PMB) 

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa** > **Konversi Pindahan (PMB)**.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-pindahan(PMB)/konversi-pindahan.png)

2. Klik ikon yang ditunjukan pada gambar tersebut.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-pindahan(PMB)/hapus-konversi-pindahan.png)

3. Klik **Ya Hapus**
# Melihat Data Konversi Pindahan (PMB)

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa > Konversi Pindahan (PMB)**.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-pindahan(PMB)/konversi-pindahan.png)

2. Klik ikon yang ditunjukan pada gambar tersebut.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-pindahan(PMB)/lihat-detail-pindahan.png)