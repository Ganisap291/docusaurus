---
sidebar position: 6
---

# Konversi Mahasiswa Pindahan 

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Konversi Mahasiswa Pindahn** ini berguna bagi kita dalam mencari  Konversi Mahasiwa Pindahan, berikut cara menggunakannya:

1. Mencari Data Konversi Mahasiswa Pindahan.

# Mencari Data Konversi Mahasiswa Pindahan

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa > **Konversi Mahasiswa Pindahan**.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-mahasiswa-pindahan/konversi-mahasiswa-pindahan.png)

2. **Masukan NIM Mahasiswa** pada search bar yang terdapat pada kiri atas.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-mahasiswa-pindahan/cari-mahasiswa-pindahan.png)