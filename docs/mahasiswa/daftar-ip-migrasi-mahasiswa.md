---
sidebar position: 8
---

# Daftar IP Migrasi Mahasiswa

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Daftar IP Migrasi Mahasiswa** ini berguna bagi kita dalam mencari Daftar IP Migarasi Mahasiswa, berikut cara menggunakannya:

1. Mencari Data Daftar IP Migrasi Mahasiswa.
2. Melihat Detail Data Daftar IP Migrasi Mahasiswa.

# Mencari Data Daftar IP Migarsi Mahasiswa

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa > **Daftar IP Migrasi Mahasiswa**.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-daftar-ip-migrasi-mahasiswa/daftar-ip-migrasi-mahasiswa.png)



# Melihat Detail Data Daftar IP Migrasi Mahasiswa

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa > Dafar IP Migrasi Mahasiswa**

![png](/img/screenshoot-menu-mahasiswa/screenshoot-daftar-ip-migrasi-mahasiswa/daftar-ip-migrasi-mahasiswa.png)

2. Klik **icon** berikut.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-daftar-ip-migrasi-mahasiswa/lihat-detail-ip-migrasi-mahasiswa.png)

