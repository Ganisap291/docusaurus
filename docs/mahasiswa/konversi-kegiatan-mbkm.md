---
sidebar position: 7
---

# Konversi Kegiatan MBKM

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Konversi Kegiatan MBKM** ini berguna bagi kita dalam mencari Konversi Kegitan MBKM, berikut cara menggunakannya:

1. Mencari Data Konversi Kegiatan MBKM.

# Mencari Data Konversi Kegiatan MBKM

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa > **Konversi Kegiatan MBKM**.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-kegiatan-mbkm/konversi-kegiatan-mbkm.png)

2. **Masukan NIM** pada search bar yang terdapat pada kiri atas.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-konversi-kegiatan-mbkm/cari-konversi-kegiatan-mbkm.png)