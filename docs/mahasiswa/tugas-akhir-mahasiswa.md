---
sidebar position: 8
---

# Tugas Akhir Mahasiswa

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Tugas Akhir Mahasiswa** ini berguna bagi kita dalam mencari Tugas Akhir Mahasiswa, berikut cara menggunakannya:

1. Mencari Tugas Akhir Mahasiswa.

# Mencari Data Tugas Akhir Mahasiswa

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa > **Tugas Akhir Mahasiswa**.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-tugas-akhir-mahasiswa/tugas-akhir-mahasiswa.png)