---
sidebar position: 3
---

# Pemrograman Matakuliah (KRS)


:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Pemogaraman Matakuliah (KRS)** ini berguna bagi kita dalam mencari Pemograman Matakuliah (KRS), berikut cara menggunakannya:

1. Mencari Data Status Mahasiswa.

# Mencari Data Status Mahasiswa

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa > **Pemograman Matakuliah (KRS)**.


![png](/img/screenshoot-menu-mahasiswa/screenshoot-pemograman-matakuliah/pemograman-matakuliah.png)


2. **Masukan NPM Mahasiswa** pada search bar yang terdapat pada kiri atas.

![png](/img/screenshoot-menu-mahasiswa/screenshoot-pemograman-matakuliah/cari-mahasiswa-berdasarkan-npm.png)