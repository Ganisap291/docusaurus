---
sidebar_position: 1
---

# Pendahuluan

Menu Mahasiswa ini berfungsi untuk membantu BAAK, Kaprodi dan Admin Prodi dalam mengelola Mahasiswa perkuliahan.

Dalam panduan ini kita akan membahas fungsi dari menu **Mahasiswasa** diantaranya :

1. Mengelola List dan Detail Mahasiswa.
2. Mengelola Pemograman Matakuliah (KRS).
3. Mengelola Persetujuan KRS.
4. Mengelola Konversi Pindahan (PMB).
5. Mengelola Konversi Mahasiswa Pindahan.
6. Mengelola Konversi Kegiatan MBKM.
7. Mengelola Daftar IP Migrasi Mahasiswa.
8. Mengelola Tugas Akhir Mahasiswa.
9. Mengelola Daftar Pengajuan Cuti.


:::note

Jika ada kekeliruan dalam dokumentasi ini mohon segera informasikan kepada kami tim SIESTA.

:::
