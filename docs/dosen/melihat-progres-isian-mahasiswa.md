---
sidebar_position: 4
---

# Melihat Progres Isian Mahasiswa

:::info
 Menu kegiatan evaluasi dosen berfungsi untuk membantu BAAK, Kaprodi dan Admin Prodi dalam melihat progress isian mahasiswa.

:::

**Persyaratan** 
1. Login Menggunakan Akun BAAK atau Admin Prodi di url https://siakad.unisma.ac.id

**Pembahasan**

Menu melihat progres isian mahasiswa ini berguna bagi kita dalam melihat progres isian mahsiswa, berikut cara menggunakannya:

1. Melihat detail isian mahasiswa

# Melihat Detail Isian Mahasiswa

Adapun caranya adalah sebagai berikut :

1. Akses menu **Dosen** > **Isian Mahasiswa** 

    ![png](/img/sreenshoot-dosen/melihat-progres-isian-mahasiswa/awalan-masuk.png)

2. Klik detail pada tabel aksi.

    ![png](/img/sreenshoot-dosen/melihat-progres-isian-mahasiswa/menuju-detail.png)

3. Setelah icon tersebut di klik, maka akan menampilkan isian mahasiswa.

    ![png](/img/sreenshoot-dosen/melihat-progres-isian-mahasiswa/isian-mahasiswa.png)