---
sidebar_position: 4
---

# Melihat Hasil Evaluasi Dosen

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu melihat evaluasi dosen ini berguna bagi kita dalam melihat evaluasi dosen, berikut cara menggunakannya:

1. Melihat Evaluasi Dosen.

# Melihat Evaluasi Dosen

Adapun caranya sebagai berikut:

1. Akses menu **Dosen > Monitoring Evaluasi Dosen**.

    ![png](/img/sreenshoot-dosen/melihat-hasil-evaluasi-dosen/awalan-masuk.png)
    
2. Klik **Detail** pada tabel aksi.

    ![png](/img/sreenshoot-dosen/melihat-hasil-evaluasi-dosen/menuju-detail.png)

3. Jika ingin keluar maka klik **Kembali**.

