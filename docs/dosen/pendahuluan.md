---
sidebar_position: 1
---

# Pendahuluan

Menu Evaluasi Dosen berfungsi untuk membantu BAAK, Kaprodi dan Admin Prodi dalam mengelola kegiatan evaluasi dosen.

Dalam panduan ini kita akan membahas fungsi dari menu **Evaluasi Dosen** diantaranya
:

1. Mengelola Kegiatan Evaluasi Dosen.
2. Mengelola Progress Isian Mahasiswa.
3. Mengelola Hasil Evaluasi Dosen.


:::note

Jika ada kekeliruan dalam dokumentasi ini mohon segera informasikan kepada kami tim SIESTA.

:::
