---
sidebar position: 2
---

# Bantuan Dosen

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Pengguna **Bantuan Dosen** ini berguna bagi kita dalam jadwal melihat mengajar dan riwayat mengajar data dosen, berikut cara menggunakannya:


1. Mencetak Jadwal Dosen.
2. Mencari Data Riwayat Mengajar Dosen.
3. Mendownload Data Riwayat Mengajar Dosen.
4. Mencetak Nilai.
5. Mencetak Rekap Nilai.

# Mecetak Jadwal Dosen

1. Akses menu **Dosen > Bantuan Dosen**.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencetak-jadwal-dosen/awalan-masuk.png)

2. Klik menu **Perkuliahan** yang terdapat pada tabel bantuan.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencetak-jadwal-dosen/menu-perkuliahan.png)

3. Klik **Jadwal** yang terdapat di kanan halaman.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencetak-jadwal-dosen/print-jadwal.png)

# Mencari Data Riwayat Mengajar Dosen

1. Akses menu **Dosen > Bantuan Dosen**.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencari-data-riwayat-mengajar-dosen/awalan-masuk.png)

2. Klik menu **Perkuliahan Pra Menara** yang terdapat pada tabel bantuan.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencari-data-riwayat-mengajar-dosen/perkuliahan-pra-menara.png)

3. Ketikan **KODE MATAKULIAH ATAU NAMA MATAKULIAH** yang ingin dicari pada search bar yang terdapat pada kanan atas.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencari-data-riwayat-mengajar-dosen/mencari-search-bar.png)

# Mendownload Nilai

1. Akses menu **Dosen > Bantuan Dosen**.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mendownload-nilai/awalan-masuk.png)

2. Klik menu **Perkuliahan Pra Menara** yang terdapat pada tabel bantuan.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mendownload-nilai/perkuliahan-pra-menara.png)

3. Klik icon yang ditunjukan pada gambar tesebut.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mendownload-nilai/aksi.png)

4. Klik menu download **Nilai** yang terdapat pada kanan atas.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mendownload-nilai/mendownload-nilai.png)

# Mencetak Nilai

1. Akses menu **Dosen > Bantuan Dosen**.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencetak-nilai/awalan-masuk.png)

2. Klik menu **Perkuliahan Pra Menara** yang terdapat pada tabel bantuan.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencetak-nilai/perkuliahan-pra-menara.png)

3. Klik icon yang ditunjukan pada gambar tesebut.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencetak-nilai/aksi.png)

4. Klik menu print **Nilai** yang terdapat pada kanan atas.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencetak-nilai/mencetak-nilai.png)


# Mencetak Rekap Nilai

1. Akses menu **Dosen > Bantuan Dosen**.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencetak-rekap-nilai/awalan-masuk.png)

2. Klik menu **Perkuliahan Pra Menara** yang terdapat pada tabel bantuan.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencetak-rekap-nilai/perkuliahan-pra-menara.png)

3. Klik icon yang ditunjukan pada gambar tesebut.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencetak-rekap-nilai/aksi.png)

4. Klik menu **Rekap Nilai** yang terdapat pada kanan atas.

    ![png](/img/sreenshoot-dosen/bantuan-dosen/mencetak-rekap-nilai/mencetak-rekap-nilai.png)
