---
sidebar position: 5
---

# Pengakuan Ekuivalensi Dosen

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Pengakuan Ekuivalensi Dosen** ini bagi kita dalam mencari, berikut cara menggunakannya:

1. Mencari Dosen
2. Mengubah Ekuivalensi

# Mencari Dosen

1. Akses menu **Dosen** > **Pengakuan Ekuivalensi Dosen**.

    ![png](/img/sreenshoot-dosen/pengakuan-ekuivalensi-dosen/awalan-masuk.png)

2. Ketikan **NAMA ATAU STATUS DOSEN** yang ingin dicari pada search bar yang terdapat diatas halaman.

    ![png](/img/sreenshoot-dosen/pengakuan-ekuivalensi-dosen/mencari-dosen.png)

# Mengubah Ekuivalensi

1. Akses menu **Dosen** > **Pengakuan Ekuivalensi Dosen**.

![png](/img/sreenshoot-dosen/pengakuan-ekuivalensi-dosen/awalan-masuk.png)


2. Klik kolom yang berisi kalimat **Tidak Ekuivalensi**.

![png](/img/sreenshoot-dosen/pengakuan-ekuivalensi-dosen/mengubah-ekuivalensi.png)

3. Ubah Ekuivalensinya.
