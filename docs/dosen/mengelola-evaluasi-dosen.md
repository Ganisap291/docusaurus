---
sidebar_position: 3
---

# Mengelola Evaluasi Dosen

:::info
Pembuatan Kegiatan ini hanya dilakukan oleh BAAK, Admin Prodi di Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu mengelola evaluasi dosen ini berguna bagi kita dalam menambah, mengubah, melihat, dan memberikan pertanyaan, berikut cara menggunakannya:

1. Menambahkan Data Evaluasi
2. Mengubah Bahan Evaluasi.
3. Menghapus Bahan Evaluasi.
4. Melihat Detail Evaluasi.
5. Memberikan Pertanyaan.

# Menambahkan Data Evaluasi
Adapun caranya sebagai berikut:

  1. Akses menu **Dosen > Evaluasi Dosen**.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/menambah-data-evaluasi/awalan-masuk.png)

  2. Klik menu **Tambah** pada halaman tersebut.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/menambah-data-evaluasi/tambah.png)

  3. Masukan data evaluasi (Thumbnail hanya opsional).

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/menambah-data-evaluasi/input-evaluasi.png)

  4. Jika telah selesai, klik **Simpan**.

# Mengubah Bahan Evaluasi
Adapaun caranya sebagai berikut:

  1. Akses menu **Dosen > Evaluasi Dosen**.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/mengubah-bahan-evaluasi/awalan-masuk.png)

  2. Klik icon berikut pada tabel aksi.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/mengubah-bahan-evaluasi/mengedit.png)

  3. Isi **Semester, Nama, Deskripsi Pembuka, Deskripsi Penutup pada halaman tersebut (Thumbnail hanya opsional)**.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/mengubah-bahan-evaluasi/mengubah-editan.png)


  4. Serta tak lupa memilih status pada halaman tersebut.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/mengubah-bahan-evaluasi/mengubah-status.png)


  5. Jika sudah maka klik **Simpan**.

# Menghapus Bahan Evaluasi
Adapun caranya sebagai berikut:

  1. Akses menu **Dosen > Evaluasi Dosen**.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/menghapus-bahan-evaluasi/awalan-masuk.png)


  2. Klik icon berikut pada tabel aksi.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/menghapus-bahan-evaluasi/menghapus.png)

  3. Pilih **Ya Hapus**.

# Melihat Data Evaluasi
Adapun caranya sebagai berikut:

  1. Akses menu **Dosen > Evaluasi Dosen**.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/melihat-data-evaluasi/awalan-masuk.png)

  2. Klik icon berikut pada tabel aksi.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/melihat-data-evaluasi/melihat.png)

  3. Maka tampilan detail evaluasi akan muncul

# Memberikan Pertanyaan
Adapun caranya sebagai berikut:

  1. Akses menu **Dosen > Evaluasi Dosen**.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/memberikan-pertanyaan/awalan-masuk.png)

  2. Klik icon berikut pada tabel aksi.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/memberikan-pertanyaan/melihat.png)

  3. Klik **Tambah Pertanyaan**.

        ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/memberikan-pertanyaan/tambah-pertanyaan.png)

  4. Masukan pertanyaan beserta tipe pertanyaannya.

      ![png](/img/sreenshoot-dosen/mengelola-evaluasi-dosen/memberikan-pertanyaan/menginput-pertanyaan.png)

  5. Jika telah selesai membuat maka klik **Simpan**.
