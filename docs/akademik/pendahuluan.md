---
sidebar_position: 1
---

# Pendahuluan

Menu Akademik ini berfungsi untuk membantu BAAK, Kaprodi dan Admin Prodi dalam mengelola akademik perkuliahan.

Dalam panduan ini kita akan membahas fungsi dari menu **Akademik** diantaranya :

1. Mengelola Kalender.
2. Mengelola Kurikulum.
3. Mengelola Tahun dan Semester.
4. Mengelola Mata Kuliah.


:::note

Jika ada kekeliruan dalam dokumentasi ini mohon segera informasikan kepada kami tim SIESTA.

:::
