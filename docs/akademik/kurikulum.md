---
sidebar position: 4
---

# Kurikulum

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Pengguna **Kurikulum** ini berguna bagi kita dalam menambah, mengubah, menghapus dan mencari data Kurikulum, berikut cara menggunakannya:

1. Menambah Data Kurikulum.
2. Mengubah Data Kurikulum.
3. Menghapus Data Kurikulum.
4. Mencari Data Kurikulum.

# Menambah Data Pengguna Kurikulum

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Pengguna Kurikulum**.

![png](/img/screenshot-menu-akademik/kurikulum/menambah-data/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.

![png](/img/screenshot-menu-akademik/kurikulum/menambah-data/tambah.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshot-menu-akademik/kurikulum/menambah-data/isi-form.png)

4. Jika telah selesai, klik **Simpan**.

# Mengubah Data Pengguna Kurikulum

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Kurikulum**.

![png](/img/screenshot-menu-akademik/kurikulum/mengubah-data/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar tesebut.

![png](/img/screenshot-menu-akademik/kurikulum/mengubah-data/ubah.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshot-menu-akademik/kurikulum/mengubah-data/isi-form.png)

4. Jika telah selesai, klik **Simpan**.

# Menghapus Data Pengguna Kurikulum

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Kurikulum**.

![png](/img/screenshot-menu-akademik/kurikulum/menghapus-data/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

![png](/img/screenshot-menu-akademik/kurikulum/menghapus-data/hapus.png)

3. Pilih **Ya Hapus**.

# Mencari Data Pengguna Kurikulum

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Kurikulum**.

![png](/img/screenshot-menu-akademik/kurikulum/mencari-data/awalan-masuk.png)

2. Ketikan **Kurikulum** pada search bar yang terdapat pada kanan atas.

![png](/img/screenshot-menu-akademik/kurikulum/mencari-data/awalan-masuk.png)
