---
sidebar position: 3
---

# Paket Kelas A

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Pengguna **Paket Kelas A** ini berguna bagi kita dalam menambah, mengubah, menghapus dan mencari data Paket Kelas A, berikut cara menggunakannya:


1. Menambah Data Paket Kelas.
2. Mengubah Data Paket Kelas.
3. Menghapus Data Paket Kelas.
4. Mencari Data Paket Kelas.
5. Menambah Matakuliah Kedalam Paket.
6. Menghapus Data Matakuliah Yang Tersedia Didalam Paket.
7. Mencari Matakuliah Yang Ingin Dimasukan Kedalam Paket.
8. Mencari Matakuliah Yang Telah Dipilih Didalam Paket.
9. Menghapus Matakuliah Yang Telah Dipilih DIdalam Paket.

# Menambah Data Paket Kelas 

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas A**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menambah-data/awalan-masuk.png)

2. Klik **Tambah** yang ada pada bagian kanan atas.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menambah-data/tambah.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menambah-data/isi-form.png)

4. Klik **Simpan**.

# Mengubah Data Paket Kelas 

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas A**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mengubah-data/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mengubah-data/ubah.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mengubah-data/isi-form.png)

4. Jika telah selesai mengisi klik **Simpan**.

# Menghapus Data Paket Kelas 

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas A**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menghapus-data/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menghapus-data/hapus.png)

3. Klik **Ya Hapus**.

# Mencari Data Paket Kelas 

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas A**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mencari-data/awalan-masuk.png)

2. Ketikan **NAMA PAKET, PROGRAM STUDI ATAU KURIKULUM** pada search bar yang terdapat pada kanan atas.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mencari-data/cari.png)

# Menambah Matakuliah Kedalam Paket

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas A**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menambah-matakuliah/awalan-masuk.png)

2. Klik **Matakuliah** yang ada pada tabel Pemetaan.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menambah-matakuliah/matakuliah.png)

3. Klik **+ Tambah Mata Kuliah Kedalam Paket**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menambah-matakuliah/tambah-paket.png)

4. Cari Matakuliah yang ingin ditambahkan yang ada pada halaman **Matakuliah Tersedia** dengan mencentang kotak yang ada di sebelah Matakuliah.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menambah-matakuliah/tambah-matakuliah.png)

5. Jika sudah, klik **Tambah** yang ada disebelah search bar.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menambah-matakuliah/tambah-matakuliah.png)

6. Klik **Simpan**.

# Menghapus Data Matakuliah Yang Tersedia Didalam Paket

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas A**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menghapus-matakuliah/awalan-masuk.png)

2. Klik **Matakuliah** yang ada pada tabel Pemetaan.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menghapus-matakuliah/matakuliah.png)

3. Klik **+ Tambah Mata Kuliah Kedalam Paket**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menghapus-matakuliah/tambah-paket.png)

4. Cari Matakuliah yang ingin ditambahkan yang ada pada halaman **Matakuliah Tersedia** dengan mencentang kotak yang ada di sebelah Data Matakuliah.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menghapus-matakuliah/tambah-matakuliah.png)

5. Jika ada yang salah dalam memilih Matakuliah, pilih Matakuliah yang terdapat pada halaman **Matakuliah Dipilih** dan centang kotak yang ada di sebelah Data Matakuliah.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menghapus-matakuliah/tambah-matkul-dipilih.png)

6. Jika sudah, klik **Hapus**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menghapus-matakuliah/hapus-matakuliah-dipilih.png)


# Mencari Matakuliah Yang Ingin Dimasukan Kedalam Paket

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas A**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mencari-matakuliah-dimasukan/awalan-masuk.png)

2. Klik **Matakuliah** yang ada pada tabel Pemetaan.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mencari-matakuliah-dimasukan/matakuliah.png)

3. Klik **+ Tambah Mata Kuliah Kedalam Paket**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mencari-matakuliah-dimasukan/tambah-paket.png)

4. Ketikan **CODE ATAU MATAKULIAH** yang ingin dicari pada search bar yang terdapat pada halaman **Matakuliah Tersedia**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mencari-matakuliah-dimasukan/cari-matakuliah.png)

# Mencari Matakuliah Yang Telah Dipilih Didalam Paket

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas A**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mencari-matakuliah-dipilih/awalan-masuk.png)

2. Klik **Matakuliah** yang ada pada tabel Pemetaan.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mencari-matakuliah-dipilih/matakuliah.png)

3. Klik **+ Tambah Mata Kuliah Kedalam Paket**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mencari-matakuliah-dipilih/tambah-paket.png)

4. Cari Matakuliah yang ingin ditambahkan yang ada pada halaman **Matakuliah Tersedia** dengan mencentang kotak yang ada di sebelah Data Matakuliah.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mencari-matakuliah-dipilih/cari-matakuliah.png)

5. Jika memilih Matakuliah terlalu banyak, anda bisa mencari Matakuliah yang telah dipilih dengan mengetik **CODE ATAU MATAKULIAH** yang ingin dicari pada search bar yang terdapat pada halaman **Matakuliah Dipilih**. 

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/mencari-matakuliah-dipilih/cari-matakuliah-dipilih.png)


# Menghapus Matakuliah Yang Telah Dipilih Didalam Paket

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas A**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menghapus-matakuliah-dipilih/awalan-masuk.png)

2. Klik **Matakuliah** yang ada pada tabel Pemetaan.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menghapus-matakuliah-dipilih/matakuliah.png)

3. Klik icon yang ditunjukan pada gambar

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-a/menghapus-matakuliah-dipilih/hapus-matakuliah.png)

4. Lalu klik **Hapus**.
