---
sidebar position: 4
---

# Paket Kelas B

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Pengguna **Paket Kelas B** ini berguna bagi kita dalam menambah, mengubah, menghapus dan mencari data Paket Kelas B, berikut cara menggunakannya:


1. Menambah Data Paket Kelas.
2. Mengubah Data Paket Kelas.
3. Menghapus Data Paket Kelas.
4. Mencari Data Paket Kelas.
5. Mencari Program Studi Paket Kelas.
6. Menambah Jadwal Pada Paket Kelas.
7. Mendistribusikan Paket Ke Mahasiswa.

# Menambah Data Paket Kelas.

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas B**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/paket-kelas-b.png)


2. Klik **Tambah** yang ada pada bagian kanan atas.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/tombol-tambah.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/tambah-paket-b.png)

4. Klik **Simpan**.

# Mengubah Data Paket Kelas.

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas B**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/paket-kelas-b.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/edit-paket-b.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/ubah-paket-b.png)

4. Jika telah selesai mengisi klik **Simpan**.

# Menghapus Data Paket Kelas.

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas B**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/paket-kelas-b.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/hapus-paket-b.png)

3. Klik **Ya Hapus**.

# Mencari Data Paket Kelas.

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas B**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/paket-kelas-b.png)

2. Ketikan **NAMA PAKET ATAU PROGRAM STUDI** pada search bar yang terdapat pada kanan atas.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/cari-data-paket-b.png)

# Mencari Program Studi Paket Kelas.

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas B**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/paket-kelas-b.png)

2. Klik **Jadwal & Mahasiswa** yang terdapat pada tabel **Pemetaan**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/jadwal-dan-mahasiswa.png)

3. Ketikan **Data** yang ingin dicari pada search bar yang terdapat pada halaman tersebut.

# Menambah Jadwal Pada Paket Kelas.

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas B**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/paket-kelas-b.png)


2. Klik **Jadwal & Mahasiswa** yang terdapat pada tabel **Pemetaan**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/jadwal-dan-mahasiswa.png)

3. Klik **Tambah Jadwal**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/tambah-jadwal.png)

# Mendistribusikan Paket Ke Mahasiswa.

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Paket Kelas B**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/paket-kelas-b.png)

2. Klik **Jadwal & Mahasiswa** yang terdapat pada tabel **Pemetaan**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/jadwal-dan-mahasiswa.png)

3. Klik **Distribusikan ke Mahasiswa**.

![png](/img/screenshot-menu-akademik/matakuliah/paket-kelas-b/distribusi-matakuliah.png)
