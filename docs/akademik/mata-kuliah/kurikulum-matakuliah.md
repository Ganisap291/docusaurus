---
sidebar position: 2
---

# Kurikulum Matakuliah

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Pengguna **Kurikulum Matakuliah** ini berguna bagi kita dalam menambah dan mencari data Kurikulum, berikut cara menggunakannya:

1. Mencari Data Kurikulum.
2. Menambah Kurikulum Matakuliah.
3. Melihat Keterangan Lintas Prodi (MBKM)

# Mencari Data Kurikulum

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Kurikulum Matakuliah**.
2. Ketikan **KURIKULUM ATAU PROGRAM STUDI** yang ingin dicari pada search bar yang terdapat pada kanan atas.

# Menambah Kurikulum Matakuliah

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Kurikulum Matakuliah**.
2. Buka menu **Matakuliah** yang terdapat pada tabel Pemetaan.
3. Klik **+ Tambah Matakuliah**. 
4. Isi **Form** yang terdapat pada halaman tersebut.
5. Klik **Simpan**.

# Melihat Keterangan Lintas Prodi (MBKM)

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Mata Kuliah > Kurikulum Matakuliah**.
2. Buka menu **Matakuliah** yang terdapat pada tabel Pemetaan.
3. Klik **Keterangan Lintas Prodi**. 
