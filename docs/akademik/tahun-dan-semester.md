---
sidebar position: 2
---

# Tahun dan Semester

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Pengguna **Tahun dan Semster** ini berguna bagi kita dalam menambah, mengubah, menghapus dan mancari data Tahun dan Semster, berikut cara menggunakannya:

1. Menambah Data Tahun dan Semester.
2. Mengubah Data Tahun dan Semester.
3. Menghapus Data Tahun dan Semester.
4. Mencari Data Tahun dan Semester.

# Menambah Data Pengguna Tahun dan Semester

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Tahun dan Semester**.

![png](/img/screenshot-menu-akademik/tahun-dan-semester/menambah-data/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.

![png](/img/screenshot-menu-akademik/tahun-dan-semester/menambah-data/tambah.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshot-menu-akademik/tahun-dan-semester/menambah-data/isi-form.png)

4. Jika telah selesai, klik **Simpan**.

# Mengubah Data Pengguna Tahun dan Semester

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Tahun dan Semester**.

![png](/img/screenshot-menu-akademik/tahun-dan-semester/mengubah-data/awalan-masuk.png)

2. Klik icon berikut:

![png](/img/screenshot-menu-akademik/tahun-dan-semester/mengubah-data/ubah.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshot-menu-akademik/tahun-dan-semester/mengubah-data/isi-form.png)

4. Jika telah selesai, klik **Simpan**.

# Menghapus Data Pengguna Tahun dan Semester

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Tahun dan Semester**.

![png](/img/screenshot-menu-akademik/tahun-dan-semester/menghapus-data/awalan-masuk.png)

2. Klik icon berikut:

![png](/img/screenshot-menu-akademik/tahun-dan-semester/menghapus-data/hapus.png)

3. Pilih **Ya Hapus**.

# Mencari Data Pengguna Tahun dan Semester

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Tahun dan Semester**.

![png](/img/screenshot-menu-akademik/tahun-dan-semester/mencari-data/awalan-masuk.png)

2. Ketikan **Tahun dan Semester** yang ingin dicari pada search bar yang terdapat pada kanan atas.

![png](/img/screenshot-menu-akademik/tahun-dan-semester/mencari-data/cari.png)
