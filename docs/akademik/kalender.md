---
sidebar position: 3
---

# Kalender

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Pengguna **Kalender** ini berguna bagi kita dalam menambah, mengubah, menghapus dan mancari data Kalender, berikut cara menggunakannya:

1. Menambah Data Kalender.
2. Menambah Data Kalender.
3. Mencari Data Kalender.
4. Menambah Data Whitelist.
5. Menambah Data Blacklist.

# Menambah Data Pengguna Kalender

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Kalender**.

![png](/img/screenshot-menu-akademik/kalender/menambah-data/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.

![png](/img/screenshot-menu-akademik/kalender/menambah-data/tambah.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshot-menu-akademik/kalender/menambah-data/isi-form.png)

4. Jika telah selesai, klik **Simpan** untuk menambahkan jadwal.

# Mengubah Data Pengguna Kalender

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Kalender**.

![png](/img/screenshot-menu-akademik/kalender/mengubah-data/awalan-masuk.png)

2. Klik icon berikut:

![png](/img/screenshot-menu-akademik/kalender/mengubah-data/ubah.png)

3. Edit **Tanggal** yang ada pada halaman tersebut.

![png](/img/screenshot-menu-akademik/kalender/mengubah-data/ubah-tanggal.png)

4. Jika telah selesai, klik **Simpan**.

# Mencari Data Pengguna Kalender

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Kalender**.

![png](/img/screenshot-menu-akademik/kalender/mencari-data/awalan-masuk.png)

2. Ketikan **Kalender** pada search bar yang terdapat pada kanan atas.

![png](/img/screenshot-menu-akademik/kalender/mencari-data/cari.png)

# Menambah Data Whitelist

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Kalender**.

![png](/img/screenshot-menu-akademik/kalender/menambah-data-whitelist/awalan-masuk.png)

2. Klik **Whitelist** pada tabel aksi.

![png](/img/screenshot-menu-akademik/kalender/menambah-data-whitelist/whitelist.png)

3. Klik **Tambah** pada pojok kanan atas.
4. Isi **Form** yang ada pada halaman tersebut.
5. Jika sudah klik **Simpan**.

# Menambah Data Blacklist

Adapun caranya sebagai berikut:

1. Akses menu **Akademik > Kalender**.
2. Klik **Blacklist** pada tabel aksi.
3. Klik **Tambah** pada pojok kanan atas.
4. Isi **Form** yang ada pada halaman tersebut.
5. Jika sudah klik **Simpan**.
