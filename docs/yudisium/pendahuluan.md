---
sidebar_position: 1
---

# Pendahuluan

Menu Menu Yudisium berfungsi untuk membantu BAAK, Kaprodi dan Admin Prodi dalam mengelola data Yudisium.

Dalam panduan ini kita akan membahas fungsi dari menu **Yudisium** diantaranya
:

1. Mengelola Data Program Studi.
2. Mengelola Data Ajuan.
3. Mengelola Data Verivikasi Ajuan.
4. Mengelola Data Keputusan Ajuan.
6. Mengelola Data Status Mahasiswa.


:::note

Jika ada kekeliruan dalam dokumentasi ini mohon segera informasikan kepada kami tim SIESTA.

:::
