---
sidebar position: 3
---

# Program Studi

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Kelompok Matakuliah** ini berguna bagi kita dalam mengubah, dan menambah data Program Studi, berikut cara menggunakannya:

1. Menambah Data Program Studi.
2. Mengubah Data Program Studi.
3. Mencari Data Program Studi.

# Menambah Data Program Studi

Adapun caranya sebagai berikut:

1. Akses menu **Yudisium** > **Program Studi**.
2. Klik **Setting Tanggal Pengajuan** pada bagian kanan atas.  
3. Isi **Tanggal Mulai dan Tanggal Akhir** yang ada pada halaman tersebut.
4. Serta tak lupa memilih status pada halaman tersebut.
5. Jika telah selesai, klik **Simpan**.

# Mengubah Data Kelompok Matakuliah 

Adapun caranya sebagai berikut:

1. Akses menu **Yudisium** > **Program Studi**.
2. Klik icon yang ditunjukan pada gambar tersebut.
3. Isi **Kode, Nama dan Singkatan** yang ada pada halaman tersebut.
4. Jika telah selesai, klik **Simpan**.


# Mencari Data Kelompok Matakuliah

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Kelompok Matakuliah**.
2. Ketikan **Kelompok Matakuliah** pada search bar yang terdapat pada kanan atas.