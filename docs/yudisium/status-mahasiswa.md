---
sidebar position: 6
---

# Status Mahasiswa

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Status Mahasiswa** ini berguna bagi kita dalam mengubah, menambah, dan melihat data Status Mahasiswa, berikut cara menggunakannya:

1. Mengubah Data Status Mahasiswa.
2. Mencari Data Status Mahasiswa.

# Mengubah Data Mahasiswa

Adapun caranya sebagai berikut:

1. Akses menu **Yudisium** > **Status Mahasiswa**.
2. Klik **Ubah Status Yudisium** pada bagian kanan atas.  
3. Jika telah selesai, klik **Simpan**.

# Melihat Data Status Mahasiswa 

Adapun caranya sebagai berikut:

1. Akses menu **Yudisium** > **Status Mahasiswa**.
2. Klik **Lihat Daftar Mahasiswa Yudisium**.


# Mencari Data Status Mahasiswa

Adapun caranya sebagai berikut:

1. Akses menu **Yudisium > **Status Mahasiswa**.
2. **Masukan NPM Mahasiswa** pada search bar yang terdapat pada kiri atas.