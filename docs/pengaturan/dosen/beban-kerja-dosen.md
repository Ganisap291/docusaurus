---
sidebar position: 2
---

# Beban Kerja Dosen

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Beban Kerja Dosenini berguna bagi kita dalam menambah, mengubah, data Beban Kerja Dosen, berikut cara menggunakannya:

1. Melihat Detail Beban Kerja Dosen.
2. Mencari Data Beban Kerja Dosen.

# Melihat Data Beban Kerja Dosen

1. Akses menu **Pengaturan > Dosen > Beban Kerja Dosen**.

![png](/img/screenshot-dosen-pengaturan/beban-kerja-dosen/beban-kerja.png)

2. Klik **Lihat Detail** pada tabel aksi.


![png](/img/screenshot-dosen-pengaturan/beban-kerja-dosen/melihat-beban-kerja.png)

# Mencari Data Beban Kerja Dosen

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Dosen > Beban Kerja Dosen**.

![png](/img/screenshot-dosen-pengaturan/beban-kerja-dosen/beban-kerja.png)

2. Ketikan **Jabatan** pada search bar di kanan.

![png](/img/screenshot-dosen-pengaturan/beban-kerja-dosen/mencari-beban-kerja.png)
