---
sidebar position: 8
---

# Pengguna SIAKAD

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

Menu Pengguna SIAKAD ini berguna bagi kita dalam menambah, mengubah, dan mancari data Pengguna SIAKAD, berikut cara menggunakannya:


1. Menambah Data Pengguna SIAKAD.
2. Mengubah Data Pengguna SIAKAD.
3. Menghapus Data Pengguna SIAKAD.
4. Mencari Data Pengguna SIAKAD.

# Menambah Data Pengguna SIAKAD

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Pengguna SIAKAD**.

    ![png](/img/screenshot-pengguna-siakad/mengubah-data/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.

    ![png](/img/screenshot-pengguna-siakad/mengubah-data/ubah.png)

3. Isi **Email Kampus**.

    ![png](/img/screenshot-pengguna-siakad/mengubah-data/isi-form.png)

4. Jika telah selesai, klik **Periksa Akun**.

# Mengubah Data Pengguna SIAKAD

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Penguna SIAKAD**.

    ![png](/img/screenshot-pengguna-siakad/mengubah-data/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-pengguna-siakad/mengubah-data/ubah.png)

3. Isi **Form** yang ada pada halaman tersebut.

    ![png](/img/screenshot-pengguna-siakad/mengubah-data/isi-form.png)

4. Jika telah selesai, klik **Simpan**.

# Menghapus Data Pengguna SIAKAD

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Penguna SIAKAD**.

    ![png](/img/screenshot-pengguna-siakad/menghapus-data/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-pengguna-siakad/menghapus-data/hapus.png)

3. Pilih **Ya Hapus**.

# Mencari Data Pengguna SIAKAD

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Pengguna SIAKAD**.

    ![png](/img/screenshot-pengguna-siakad/mencari-data/awalan-masuk.png)

2. Ketikan **NAMA / EMAIL** pada search bar yang terdapat pada kanan atas.

    ![png](/img/screenshot-pengguna-siakad/mencari-data/cari.png)
