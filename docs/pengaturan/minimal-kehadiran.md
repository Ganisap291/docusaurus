---
sidebar_position: 7
---

# Minimal Kehadiran
:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Minimal Kehadiran ini berguna bagi kita dalam mengubah data Minimal Kehadiran, berikut cara menggunakannya:

1. Mengubah Minimal Kehadiran.

# Mengubah Data Minimal Kehadiran

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Minimal Kehadiran**.

    ![png](/img/screenshot-minimal-kehadiran/awalan-masuk.png)

2. Klik **Ubah** pada tabel aksi.

    ![png](/img/screenshot-minimal-kehadiran/ubah.png)

3. Isi **Minimal Kehadiran**.

    ![png](/img/screenshot-minimal-kehadiran/isi-form.png)

4. Jika sudah klik **Simpan**.

