---
sidebar position: 8
---
# Distribusi Dosen

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Distribusi Dosen** ini berguna bagi kita dalam mengubah data Jenjang Pendidikan, berikut cara menggunakannya:

1. Mencari Data Distribusi Dosen.
2. Mengubah Data Distribusi Dosen.

# Mencari Data Distribusi Dosen

Adapun caranya sebagai berikut: 

1. Akses menu **Pengaturan > Distribusi Dosen**.

    ![png](/img/screenshot-distribusi-dosen/mencari-distribusi-dosen/awalan-masuk.png)

2. Ketikan **NAMA** pada search bar yang terdapat pada kanan atas.

    ![png](/img/screenshot-distribusi-dosen/mencari-distribusi-dosen/mencari.png)


# Mengubah Data Distribusi Dosen

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Distribusi Dosen**.

    ![png](/img/screenshot-distribusi-dosen/mengubah-distribusi-dosen/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

    ![png](/img/screenshot-distribusi-dosen/mengubah-distribusi-dosen/mengubah.png)

3. Ubah **NIP, NAMA LENGKAP DAN PROGRAM STUDI** yang ada pada halaman tersebut.

    ![png](/img/screenshot-distribusi-dosen/mengubah-distribusi-dosen/isi-form.png)

4. Jika telah selesai maka klik **SIMPAN**.

