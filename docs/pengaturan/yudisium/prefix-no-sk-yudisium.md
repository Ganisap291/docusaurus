---
sidebar position: 3
---

# Prefix No SK Yudisium

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Prefix No SK Yudisium ini berguna bagi kita dalam menambah, mengubah, dan mancari data Prefix No SK Yudisium, berikut cara menggunakannya:


1. Menambah Data Pefix No SK Yudisium.
2. Mengubah Data Prefix No SK Yudisium.
3. Menghapus Data Prefix No Sk Yudisum.
4. Mencari Data Prefix No SK Yudisium.

# Menambah Data Prefix No SK Yudisium

1. Akses menu **Pengaturan > Yudisium > Prefix No SK Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-prefix-no-sk-yudisium/prefix-no-sk-yudisium.png)

2. Klik **Tambah** pada bagian kanan atas.

![png](/img/screenshoot-yudisium-pengaturan/ss-prefix-no-sk-yudisium/prefix-no-sk-yudisium-tambah.png)

3. Isi **Prefix**.

![png](/img/screenshoot-yudisium-pengaturan/ss-prefix-no-sk-yudisium/tambah-prefix-no-sk-yudisium.png)

4. Serta tak lupa memilih status pada halaman tersebut.
5. Jika telah selesai, klik **Simpan**.

# Mengubah Data Prefix No SK Yudisium

1. Akses menu **Pengaturan > Yudisium > Prefix No SK Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-prefix-no-sk-yudisium/prefix-no-sk-yudisium.png)

2. Klik icon berikut:

![png](/img/screenshoot-yudisium-pengaturan/ss-prefix-no-sk-yudisium/prefix-no-sk-yudisium-edit.png)

3. Isi **Prefix**.

![png](/img/screenshoot-yudisium-pengaturan/ss-prefix-no-sk-yudisium/edit-prefix-no-sk-yudisium.png)

4. Serta tak lupa memilih status pada halaman tersebut.
5. Jika telah selesai, klik **Simpan**.

# Menghapus Data Prefix No SK Yudisium

1. Akses menu **Pengaturan > Yudisium > Prefix No SK Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-prefix-no-sk-yudisium/prefix-no-sk-yudisium.png)

2. Klik icon berikut:

![png](/img/screenshoot-yudisium-pengaturan/ss-prefix-no-sk-yudisium/prefix-no-sk-yudisium-hapus.png)

3. Pilih **Ya Hapus**.

# Mencari Data Prefix No SK Yudisium

1. Akses menu **Pengaturan > Yudisium > Prefix No SK Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-prefix-no-sk-yudisium/prefix-no-sk-yudisium.png)

2. Ketikan **Prefix** pada search bar yang terdapat pada kanan atas.

![png](/img/screenshoot-yudisium-pengaturan/ss-prefix-no-sk-yudisium/prefix-no-sk-yudisium-cari.png)
