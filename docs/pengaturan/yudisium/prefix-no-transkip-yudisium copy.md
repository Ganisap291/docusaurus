---
sidebar position: 4
---

# Prefix No Transkip 

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

Menu Prefix No Transkip ini berguna bagi kita dalam menambah, mengubah, dan mancari data Prefix No Transkip, berikut cara menggunakannya:


1. Menambah Data Pefix No Transkip.
2. Mengubah Data Prefix No Transkip.
3. Menghapus Data Prefix No Transkip.
4. Mencari Data Prefix No Transkip.

# Menambah Data Prefix No Transkip

1. Akses menu **Pengaturan > Yudisium > Prefix No Transkip**.



2. Klik **Tambah** pada bagian kanan atas.

    ![png](/img/screenshot-prefik-no-transkrip/menambah-data/awalan-masuk.png)

3. Isi **Form** yang ada pada halaman tersebut.

    ![png](/img/screenshot-prefik-no-transkrip/menambah-data/tambah.png)

4. Jika telah selesai, klik **Simpan**.

# Mengubah Data Prefix No Transkip

1. Akses menu **Pengaturan > Yudisium > Prefix No Transkip**.

    ![png](/img/screenshot-prefik-no-transkrip/mengubah-data/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-prefik-no-transkrip/mengubah-data/ubah.png)

3. Isi **Form** yang ada pada halaman tersebut.

    ![png](/img/screenshot-prefik-no-transkrip/mengubah-data/isi-form.png)

4. Jika telah selesai, klik **Simpan**.

# Menghapus Data Prefix No Transkip

1. Akses menu **Pengaturan > Yudisium > Prefix No Transkip**.

    ![png](/img/screenshot-prefik-no-transkrip/menghapus-data/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-prefik-no-transkrip/menghapus-data/hapus.png)

3. Pilih **Ya Hapus**.

# Mencari Data Prefix No Transkip

1. Akses menu **Pengaturan > Yudisium > Prefix No Transkip**.

    ![png](/img/screenshot-prefik-no-transkrip/mencari-data/awalan-masuk.png)

2. Ketikan **Prefix** pada search bar yang terdapat pada kanan atas.

    ![png](/img/screenshot-prefik-no-transkrip/mencari-data/cari.png)
