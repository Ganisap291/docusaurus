---
sidebar position: 2
---

# Periode Yudisium 

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Periode Yudisium ini berguna bagi kita dalam menambah, mengubah, dan mancari data Periode Yudisium, berikut cara menggunakannya:


1. Menambah Data Periode Yudisium.
2. Mengubah Data Periode Yudisium.
3. Menghapus Data Periode Yudisum.
4. Mencari Data Periode Yudisium.

# Menambah Data Periode Yudisium

1. Akses menu **Pengaturan > Yudisium > Periode Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-periode-yudisium/periode-yudisium.png)

2. Klik **Tambah** pada bagian kanan atas.

![png](/img/screenshoot-yudisium-pengaturan/ss-periode-yudisium/tambah-perioe-yudisium.png)

3. Isi **Periode, Pilih Bulan, Pilih Periode Semester**.

![png](/img/screenshoot-yudisium-pengaturan/ss-periode-yudisium/isi-periode-yudisium.png)

4. Serta tak lupa memilih status pada halaman tersebut.
5. Jika telah selesai, klik **Simpan**.

# Mengubah Data Periode Yudisium

1. Akses menu **Pengaturan > Yudisium > Periode Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-periode-yudisium/periode-yudisium.png)

2. Klik icon berikut:

![png](/img/screenshoot-yudisium-pengaturan/ss-periode-yudisium/edit-periode-yudisium.png)

3. Isi **Periode, Pilih Bulan, Pilih Periode Semester**.

![png](/img/screenshoot-yudisium-pengaturan/ss-periode-yudisium/edit-isi-periode-yudisium.png)

4. Serta tak lupa memilih status pada halaman tersebut.
5. Jika telah selesai, klik **Simpan**.

# Menghapus Data Periode Yudisium

1. Akses menu **Pengaturan > Yudisium > Periode Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-periode-yudisium/periode-yudisium.png)

2. Klik icon berikut:

![png](/img/screenshoot-yudisium-pengaturan/ss-periode-yudisium/hapus-periode-yudisium.png)

3. Pilih **Ya Hapus**.

# Mencari Data Jenis Kelas

1. Akses menu **Pengaturan > Yudisium > Periode Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-periode-yudisium/periode-yudisium.png)

2. Ketikan **PERIODE KE / BULAN / PERIODE SEMESTER** pada search bar yang terdapat pada kanan atas.

![png](/img/screenshoot-yudisium-pengaturan/ss-periode-yudisium/cari-peiode-yudisium.png)