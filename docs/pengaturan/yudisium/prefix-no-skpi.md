---
sidebar position: 5
---

# Prefix No SKPI

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

Menu Prefix No SKPI ini berguna bagi kita dalam menambah, mengubah, dan mancari data Prefix No SKPI, berikut cara menggunakannya:


1. Menambah Data Pefix No SKPI.
2. Mengubah Data Prefix No SKPI.
3. Menghapus Data Prefix No SKPI.
4. Mencari Data Prefix No SKPI.

# Menambah Data Prefix No SKPI

1. Akses menu **Pengaturan > Yudisium > Prefix No SKPI**.

    ![png](/img/screenshot-prefix-no-skpi/menambah-data/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.

    ![png](/img/screenshot-prefix-no-skpi/menambah-data/tambah.png)

3. Isi **Form** yang ada pada halaman tersebut.

    ![png](/img/screenshot-prefix-no-skpi/menambah-data/isi-form.png)

4. Jika telah selesai, klik **Simpan**.

# Mengubah Data Prefix No SKPI

1. Akses menu **Pengaturan > Yudisium > Prefix No SKPI**.

    ![png](/img/screenshot-prefix-no-skpi/mengubah-data/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-prefix-no-skpi/mengubah-data/ubah.png)

3. Isi **Form** yang ada pada halaman tersebut.

    ![png](/img/screenshot-prefix-no-skpi/mengubah-data/isi-form.png)

5. Jika telah selesai, klik **Simpan**.

# Menghapus Data Prefix No SKPI

1. Akses menu **Pengaturan > Yudisium > Prefix No SKPI**.

    ![png](/img/screenshot-prefix-no-skpi/menghapus-data/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-prefix-no-skpi/menghapus-data/hapus.png)

3. Pilih **Ya Hapus**.

# Mencari Data Prefix No SKPI

1. Akses menu **Pengaturan > Yudisium > Prefix No SKPI**.

    ![png](/img/screenshot-prefix-no-skpi/mencari-data/awalan-masuk.png)

2. Ketikan **Prefix** pada search bar yang terdapat pada kanan atas.

    ![png](/img/screenshot-prefix-no-skpi/mencari-data/cari.png)
