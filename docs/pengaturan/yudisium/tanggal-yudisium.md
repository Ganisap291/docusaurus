---
sidebar position: 1
---

# Tanggal Yudisium

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

Menu Tanggal Yudisium ini berguna bagi kita dalam menambah, mengubah, dan mancari data Tanggal Yudisium, berikut cara menggunakannya:


1. Menambah Data Tanggal Yudisium.
2. Mengubah Data Tanggal Yudisium.
3. Menghapus Data Tanggal Yudisium.
4. Mencari Data Tanggal Yudisium.

# Menambah Data Tanggal Yudisium

1. Akses menu **Pengaturan > Yudisium > Tanggal Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-tanggal-yudisium/tanggal-yudisium.png)

2. Klik **Tambah** pada bagian kanan atas.

![png](/img/screenshoot-yudisium-pengaturan/ss-tanggal-yudisium/tombol-tambah.png)

3. Isi **Tanggal**.

![png](/img/screenshoot-yudisium-pengaturan/ss-tanggal-yudisium/tambah-tanggal-yudisium.png)

4. Serta tak lupa memilih status pada halaman tersebut.
5. Jika telah selesai, klik **Simpan**.

# Mengubah Data Tanggal Yudisium

1. Akses menu **Pengaturan > Yudisium > Tanggal Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-tanggal-yudisium/tanggal-yudisium.png)

2. Klik icon berikut:

![png](/img/screenshoot-yudisium-pengaturan/ss-tanggal-yudisium/edit-tanggal-yudisium.png)

3. Isi **Tanggal**.

![png](/img/screenshoot-yudisium-pengaturan/ss-tanggal-yudisium/ubah-tanggal-yudisium.png)

4. Serta tak lupa memilih status pada halaman tersebut.
5. Jika telah selesai, klik **Simpan**.

# Menghapus Data Tanggal Yudisium

1. Akses menu **Pengaturan > Yudisium > Tanggal Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-tanggal-yudisium/tanggal-yudisium.png)

2. Klik icon berikut:

![png](/img/screenshoot-yudisium-pengaturan/ss-tanggal-yudisium/hapus-tanggal-yudisium.png)

3. Pilih **Ya Hapus**.

# Mencari Data Tanggal Yudisium

1. Akses menu **Pengaturan > Yudisium > Tanggal Yudisium**.

![png](/img/screenshoot-yudisium-pengaturan/ss-tanggal-yudisium/tanggal-yudisium.png)

2. Ketikan **Tanggal** pada search bar yang terdapat pada kanan atas.

![png](/img/screenshoot-yudisium-pengaturan/ss-tanggal-yudisium/cari-tanggal-yudisium.png)