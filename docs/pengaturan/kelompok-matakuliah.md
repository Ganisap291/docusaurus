---
sidebar_position: 8
---

# Kelompok Matakuliah

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Kelompok Matakuliah** ini berguna bagi kita dalam mengubah, menambah, mengahpus data Kelompk Mata Kuliah, berikut cara menggunakannya:

1. Menambah Data Kelompok Matakuliah.
2. Mengubah Data Kelompok Matakuliah.
3. Menghapus Data Kelompok Matakuliah.
4. Mencari Data Kelompok Matakuliah.

# Menambah Data Kelompok Matakuliah

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Kelompok Matakuliah**.

    ![png](/img/screenshot-kelompok-matakuliah/menambah-kelompok-matakuliah/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.

    ![png](/img/screenshot-kelompok-matakuliah/menambah-kelompok-matakuliah/tambah.png)

3. Isi **Form** yang ada pada halaman tersebut.

    ![png](/img/screenshot-kelompok-matakuliah/menambah-kelompok-matakuliah/isi-form.png)

4. Jika telah selesai, klik **Simpan**.

# Mengubah Data Kelompok Matakuliah 

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Kelompok Matakuliah**.

    ![png](/img/screenshot-kelompok-matakuliah/mengubah-kelompok-matakuliah/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

    ![png](/img/screenshot-kelompok-matakuliah/mengubah-kelompok-matakuliah/ubah.png)

3. Isi **Kode, Nama dan Singkatan** yang ada pada halaman tersebut.

    ![png](/img/screenshot-kelompok-matakuliah/mengubah-kelompok-matakuliah/isi-form.png)

4. Jika telah selesai, klik **Simpan**.

# Menghapus Data Kelompok Matakuliah

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Kelompok Matakuliah**.

    ![png](/img/screenshot-kelompok-matakuliah/menghapus-kelompok-matakuliah/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

    ![png](/img/screenshot-kelompok-matakuliah/menghapus-kelompok-matakuliah/hapus.png)

3. Pilih **Ya Hapus**.

# Mencari Data Kelompok Matakuliah

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Kelompok Matakuliah**.

    ![png](/img/screenshot-kelompok-matakuliah/mencari-kelompok-matakuliah/awalan-masuk.png)

2. Ketikan **Kelompok Matakuliah** pada search bar yang terdapat pada kanan atas.

    ![png](/img/screenshot-kelompok-matakuliah/mencari-kelompok-matakuliah/cari.png)
