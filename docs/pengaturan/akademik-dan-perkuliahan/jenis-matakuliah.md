---
sidebar position: 2
---

# Jenis Matakuliah

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Jenis Matakuliah ini berguna bagi kita dalam menambah, mengubah, data Jenis Matakuliah, berikut cara menggunakannya:

1. Menambah Jenis Matakuliah.
2. Mengubah Jenis Matakuliah.
3. Menghapus Jenis Matakuliah.
4. Mencari Jenis Matakuliah.

# Menambah Jenis Matakuliah

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Jenis Matakuliah**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-matakuliah/menambah-jenis-matakuliah/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-matakuliah/menambah-jenis-matakuliah/tambah.png)

3. Isi **Kode dan Nama Matakuliah**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-matakuliah/menambah-jenis-matakuliah/isi-form.png)

4. Jika sudah klik **Simpan**.

# Mengubah Jenis Matakuliah

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Jenis Matakuliah**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-matakuliah/mengubah-jenis-matakuliah/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-matakuliah/mengubah-jenis-matakuliah/ubah.png)

3. Edit **Kode dan Nama Matakuliah**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-matakuliah/mengubah-jenis-matakuliah/isi-form.png)

4. Jika sudah klik **Simpan**.

# Menghapus Jenis Matakuliah

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Jenis Matakuliah**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-matakuliah/menghapus-jenis-matakuliah/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-matakuliah/menghapus-jenis-matakuliah/hapus.png)

3. Klik **Ya Hapus**

# Mencari Jenis Matakuliah

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Jenis Matakuliah**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-matakuliah/mencari-jenis-matakuliah/awalan-masuk.png)

2. Klik **Search Bar** pada halaman tersebut

    ![png](/img/screenshot-akademik-perkuliahan/jenis-matakuliah/mencari-jenis-matakuliah/cari.png)

3. Ketikan **KODE / NAMA** pada Search Bar yang terdapat pada kanan atas