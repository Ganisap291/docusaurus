---
sidebar position: 3
---

# Minimal Kuota Terisi

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Minimal Kuota Terisi** ini berguna bagi kita dalam mengubah data Minimal Kuota, berikut cara menggunakannya:

1. Mengubah Minimal Kuota Terisi.

# Mengubah Minimal Kuota Terisi

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Minimal Kuota Terisi**.

    ![png](/img/screenshot-akademik-perkuliahan/minimal-kuota-terisi/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar.

    ![png](/img/screenshot-akademik-perkuliahan/minimal-kuota-terisi/ubah.png)

3. Isi form yang pada halaman tersebut.

    ![png](/img/screenshot-akademik-perkuliahan/minimal-kuota-terisi/isi-form.png)

4. Jika telah mengisi form, maka klik **Simpan**
