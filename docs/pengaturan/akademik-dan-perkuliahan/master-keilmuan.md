---
sidebar position: 8
---

# Master Keilmuan

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Master Keilmuan ini berguna bagi kita dalam mancari data Keilmuan, berikut cara menggunakannya:

1. Mencari Data Pada Master Keilmuan.

# Mencari Data Master Keilmuan

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master Keilmuan**.

    ![png](/img/screenshot-akademik-perkuliahan/master-keilmuan/awalan-masuk.png)

2. Ketikan **KODE / NAMA** pada search bar yang terdapat pada kanan atas.

    ![png](/img/screenshot-akademik-perkuliahan/master-keilmuan/cari.png)
