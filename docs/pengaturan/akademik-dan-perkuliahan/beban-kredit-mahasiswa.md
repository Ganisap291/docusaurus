---
sidebar_position: 1
---

# Beban Kredit Mahasiswa

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Beban Kredit Mahasiswa ini berguna bagi kita dalam mengubah data Beban Kredit Mahasiswa, berikut cara menggunakannya:

1. Mengubah Data Beban Kredit Mahasiswa.

# Mengubah Data Beban Kredit Mahasiswa

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Beban Kredit Mahasiswa**.

    ![png](/img/screenshot-akademik-perkuliahan/beban-kredit-mahasiswa/awalan-masuk.png)
 
2. Klik **Ubah** pada tabel aksi.

    ![png](/img/screenshot-akademik-perkuliahan/beban-kredit-mahasiswa/ubah.png)

3. Isi **Minimal IP Semester, Maksimal IP Semester, Minimal Pengambilan Beban SKS, Maksimal Pengambilan SKS**.

    ![png](/img/screenshot-akademik-perkuliahan/beban-kredit-mahasiswa/isi-form.png)

4. Jika sudah klik **Simpan**.


