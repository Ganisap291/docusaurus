---
sidebar position: 7
---

# Master Ruangan

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Master Ruangan ini berguna bagi kita dalam menambah, mengubah, dan mancari data Ruangan, berikut cara menggunakannya:

1. Menambah Data Master Ruangan.
2. Mengubah Data Master Ruangan.
3. Menghapus Data Master Ruangan.
4. Mencari Data Master Ruangan.

# Menambah Data Master Ruangan

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master Ruangan**.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/menambah-master-ruangan/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/menambah-master-ruangan/tambah.png)

3. Isi **Nama Ruangan, Nama Gedung, No Lantai, Fakultas, Kegunaan**.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/menambah-master-ruangan/isi-form.png)

4. Pada pengsian **Fakultas** kita bisa menambah data sebanyak yang kita mau dengan mengklik tombol berikut.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/menambah-master-ruangan/fakultas.png)

5. Serta tak lupa memilih status pada halaman tersebut.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/menambah-master-ruangan/status.png)

6. Jika telah selesai, klik **Simpan**.

# Mengubah Data Master Ruangan

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master Ruangan**.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/mengubah-master-ruangan/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/mengubah-master-ruangan/ubah.png)

3. Isi **Nama Ruangan, Nama Gedung, No Lantai, Fakultas, Kegunaan**.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/mengubah-master-ruangan/isi-form.png)

4. Pada pengsian **Fakultas** kita bisa menambah data sebanyak yang kita mau dengan mengklik tombol berikut.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/mengubah-master-ruangan/fakultas.png)

5. Serta tak lupa memilih status pada halaman tersebut.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/mengubah-master-ruangan/status.png)

6. Jika telah selesai, klik **Simpan**.

# Menghapus Data Master Ruangan

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master Ruangan**.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/menghapus-master-ruangan/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/menghapus-master-ruangan/hapus.png)

3. Pilih **Ya Hapus**.

# Mencari Data Master Ruangan

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Jenis Kelas**.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/mencari-master-ruangan/awalan-masuk.png)

2. Ketikan **NAMA RUANGAN** pada search bar yang terdapat pada kanan atas.

    ![png](/img/screenshot-akademik-perkuliahan/master-ruangan/mencari-master-ruangan/cari.png)

