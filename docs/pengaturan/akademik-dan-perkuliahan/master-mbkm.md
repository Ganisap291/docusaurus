---
sidebar position: 9
---

# Master MBKM

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Master MBKM ini berguna bagi kita dalam menambah, mengubah, dan mancari data MBKM, berikut cara menggunakannya:

1. Menambah Data Master MBKM.
2. Mengubah Data Master MBKM.
3. Menghapus Data Master MBKM.
4. Mencari Data Master MBKM.

# Menambah Data Master MBKM

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master MBKM**.

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/menambah-data-master/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/menambah-data-master/menambah.png)

3. Isi **Tipe Kegiatan, Kriteria Penilaian, Masukan Outcome**.

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/menambah-data-master/isi-form.png)

4. Pada pengsian **Kriteria Penilaian** kita bisa menambah data sebanyak yang kita mau dengan mengklik tombol berikut.

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/menambah-data-master/tambah-kriteria-penilaian.png)

5. Jika telah selesai, klik **Simpan**.

# Mengubah Data Master MBKM 

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master MBKM**.

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/mengubah-data-master/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/mengubah-data-master/mengubah.png)

3. Isi **Tipe Kegiatan, Kriteria Penilaian, Masukan Outcome**.

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/mengubah-data-master/isi-form.png)

4. Pada pengsian **Kriteria Penilaian** kita bisa menambah data sebanyak yang kita mau dengan mengklik tombol berikut.

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/mengubah-data-master/tambah-kriteria-penilaian.png)

4. Jika telah selesai, klik **Simpan**.

# Menghapus Data Master MBKM

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master MBKM**.

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/menghapus-data-master/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/menghapus-data-master/menghapus.png)

3. Pilih **Ya Hapus**.

# Mencari Data Master MBKM

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master MBKM**.

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/mencari-data-master/awalan-masuk.png)

2. Ketikan **Tipe, Kriteria Penilaian, Outcome** pada search bar yang terdapat pada kanan atas.

    ![png](/img/screenshot-akademik-perkuliahan/master-mbkm/mencari-data-master/mencari.png)

