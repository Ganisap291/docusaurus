---
sidebar position: 6
---

# Master Sesi

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

1. Menambah Master Sesi.
2. Mengubah Master Sesi.
3. Menghapus Master Sesi.
4. Mencari Master Sesi.

# Menambah Sesi

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master Sesi**.

    ![png](/img/screenshot-akademik-perkuliahan/master-sesi/menambah-sesi/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.

    ![png](/img/screenshot-akademik-perkuliahan/master-sesi/menambah-sesi/menambah.png)

3. Isi form yang ada di halaman tersebut.

    ![png](/img/screenshot-akademik-perkuliahan/master-sesi/menambah-sesi/isi-form.png)

4. Jika sudah klik **Simpan**.

# Mengubah Sesi

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master Sesi**.

    ![png](/img/screenshot-akademik-perkuliahan/master-sesi/mengubah-sesi/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar.

    ![png](/img/screenshot-akademik-perkuliahan/master-sesi/mengubah-sesi/mengubah.png)

3. Edit **Kode dan Nama Matakuliah**.

    ![png](/img/screenshot-akademik-perkuliahan/master-sesi/mengubah-sesi/isi-form.png)

4. Jika sudah klik **Simpan**.

# Menghapus Sesi

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master Sesi**.

    ![png](/img/screenshot-akademik-perkuliahan/master-sesi/menghapus-sesi/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar.

    ![png](/img/screenshot-akademik-perkuliahan/master-sesi/menghapus-sesi/menghapus.png)

3. Klik **Ya Hapus**

# Mencari Sesi

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master Sesi**.

    ![png](/img/screenshot-akademik-perkuliahan/master-sesi/mencari-sesi/awalan-masuk.png)

2. Klik **Search Bar** pada halaman tersebut

    ![png](/img/screenshot-akademik-perkuliahan/master-sesi/mencari-sesi/mencari.png)

3. Ketikan **KODE / NAMA** pada Search Bar yang terdapat pada kanan atas