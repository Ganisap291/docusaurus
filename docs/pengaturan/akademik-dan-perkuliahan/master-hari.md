---
sidebar position: 4
---

# Master Hari

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Master Hari ini berguna bagi kita dalam mancari data Hari, berikut cara menggunakannya:

1. Mencari Data Hari.

# Mencari Data Hari

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Master Hari**.

    ![png](/img/screenshot-akademik-perkuliahan/master-hari/awalan-masuk.png)

2. Klik **Search Bar** pada halaman tersebut.

    ![png](/img/screenshot-akademik-perkuliahan/master-hari/cari.png)

3. Ketikan **HARI** pada Search Bar yang terdapat pada kanan atas.
