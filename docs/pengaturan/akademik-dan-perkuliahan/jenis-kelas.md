---
sidebar position: 4
---

# Jenis Kelas

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Jenis Kelas ini berguna bagi kita dalam menambah, mengubah, dan mancari data Jenis Kelas, berikut cara menggunakannya:


1. Menambah Data Jenis Kelas.
2. Mengubah Data Jenis Kelas.
3. Menghapus Data Jenis Kelas.
4. Mencari Data Jenis Kelas.

# Menambah Data Jenis Kelas

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Jenis Kelas**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-kelas/menambah-data-kelas/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-kelas/menambah-data-kelas/tambah.png)

3. Isi **Nama Kelas, Kode Kelas, Alias**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-kelas/menambah-data-kelas/isi-form.png)

4. Jika telah selesai, klik **Simpan**.

# Mengubah Data Jenis Kelas 

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Jenis Kelas**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-kelas/mengubah-data-jenis-kelas/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-akademik-perkuliahan/jenis-kelas/mengubah-data-jenis-kelas/ubah.png)

3. Isi **Kode, Nama**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-kelas/mengubah-data-jenis-kelas/isi-form.png)

4. Jika telah selesai, klik **Simpan**.

# Menghapus Data Jenis Kelas

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Jenis Kelas**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-kelas/menghapus-data-jenis-kelas/awalan-masuk.png)

2. Klik icon berikut:

    ![png](/img/screenshot-akademik-perkuliahan/jenis-kelas/menghapus-data-jenis-kelas/hapus.png)

3. Pilih **Ya Hapus**.

# Mencari Data Jenis Kelas

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Akademik dan Perkuliahan > Jenis Kelas**.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-kelas/mencari-data-jenis-kelas/awalan-masuk.png)

2. Ketikan **KODE KELAS / NAMA KELAS** pada search bar yang terdapat pada kanan atas.

    ![png](/img/screenshot-akademik-perkuliahan/jenis-kelas/mencari-data-jenis-kelas/cari.png)
