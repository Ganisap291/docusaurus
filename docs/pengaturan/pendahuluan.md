---
sidebar_position: 1
---

# Pendahuluan

Menu Pengaturan berfungsi untuk membantu BAAK, Kaprodi dan Admin Prodi dalam mengelola data master.

Dalam panduan ini kita akan membahas fungsi dari menu **Pengaturan** diantaranya
:

1. Mengelola Data Master Akademik dan Perkuliahan.
2. Mengelola Data Master Dosen.
3. Mengelola Data Master Penilaian.
4. Mengelola Data Master Unit Pendidikan.
6. Mengelola Data Master Yudisium.
7. Mengelola Data Master Distribusi Dosen.
8. Mengelola Data Master Kelompok Matakuliah.
9. Mengelola Data Master Minimal Kehadiran.
9. Mengelola Data Master Pengguna SIAKAD.



:::note

Jika ada kekeliruan dalam dokumentasi ini mohon segera informasikan kepada kami tim SIESTA.

:::
