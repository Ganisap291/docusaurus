---
sidebar position: 3
---

# Bobot Nilai

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Bobot Nilai** ini berguna bagi kita dalam menambah, mengubah, menghapus dan mencari Bobot Nilai, berikut cara menggunakannya:

Adapun caranya sebagai berikut:

1. Menambah Data Bobot Nilai.
2. Mengubah Data Bobot Nilai.
3. Menghapus Data Bobot Nilai.
4. Mencari Data Bobot Nilai.

# Menambah Data Bobot Nilai

Adapun caranya sebagai berikut :

1. Akses menu **Pengaturan > Penilaian > Bobot Nilai**.

![png](/img/screenshoot-penilaian/bobot-nilai/bobot-nilai.png)

2. Klik **Tambah** pada bagian kanan atas.

![png](/img/screenshoot-penilaian/bobot-nilai/tambah-bobot-nilai.png)

3. Isi **form** yang ada pada halaman tersebut.

![png](/img/screenshoot-penilaian/bobot-nilai/isi-form-tambah.png)

4. Jika telah selesai, klik **Simpan**.

# Mengubah Data Bobot Nilai 

Adapun caranya sebagai berikut :

1. Akses menu **Pengaturan > Penilaian > Bobot Nilai**.

![png](/img/screenshoot-penilaian/bobot-nilai/bobot-nilai.png)

2. Klik icon yang ditunjukan pada gambar.

![png](/img/screenshoot-penilaian/bobot-nilai/edit-bobot-nilai.png)

3. Isi **form** yang ada pada halaman tersebut.

![png](/img/screenshoot-penilaian/bobot-nilai/edit-form.png)

4. Jika telah selesai, klik **Simpan**.

# Menghapus Data Bobot Nilai

Adapun caranya sebagai berikut :

1. Akses menu **Pengaturan > Penilaian > Bobot Nilai**.

![png](/img/screenshoot-penilaian/bobot-nilai/bobot-nilai.png)

2. Klik icon yang ditunjukan pada gambar. 

![png](/img/screenshoot-penilaian/bobot-nilai/hapus-bobot-nilai.png)

3. Pilih **Ya Hapus**.

# Mencari Data Bobot Nilai

Adapun caranya sebagai berikut :

1. Akses menu **Pengaturan > Penilaian > Bobot Nilai**.

![png](/img/screenshoot-penilaian/bobot-nilai/bobot-nilai.png)

2. Ketikan **NAMA** pada search bar yang terdapat pada kanan atas.

![png](/img/screenshoot-penilaian/bobot-nilai/cari-bobot-nilai.png)
