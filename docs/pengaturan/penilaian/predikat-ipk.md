---
sidebar position: 2
---

# Predikat IPK

:::info
 Menu kegiatan evaluasi dosen berfungsi untuk membantu BAAK, Kaprodi dan Admin Prodi dalam melihat progress isian mahasiswa.

:::

**Persyaratan** 
1. Login Menggunakan Akun BAAK atau Admin Prodi di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Predikat IPK** ini berguna bagi kita dalam mengubah data Predikat IPK, berikut cara menggunakannya:

1. Mengubah Predikat Nilai IPK

# Mengubah Predikat Nilai IPK

Adapun caranya adalah sebagai berikut :

1. Akses menu **Pengaturan > Isian Mahasiswa > Penilaian > Predikat IPK**. 

![png](/img/screenshoot-penilaian/mengubah-ipk/predikat-ipk.png)

2. Klik **Ubah**.

![png](/img/screenshoot-penilaian/mengubah-ipk/ubah-predikat-ipk.png)

3. Isi **Form** pada halaman tersebut.

![png](/img/screenshoot-penilaian/mengubah-ipk/isi-form-ipk.png)

4. Jika telah selesai mengubah, klik **Simpan**.