---
sidebar_position: 1
---

# Predikat Nilai Matakuliah

:::info
 Menu kegiatan evaluasi dosen berfungsi untuk membantu BAAK, Kaprodi dan Admin Prodi dalam melihat progress isian mahasiswa.

:::

**Persyaratan** 
1. Login Menggunakan Akun BAAK atau Admin Prodi di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Predikat Nilai Matakuliah** ini berguna bagi kita dalam mengubah data Predikat Nilai, berikut cara menggunakannya:

1. Mengubah Predikat Nilai Matakuliah

# Mengubah Predikat Nilai Matakuliah

Adapun caranya adalah sebagai berikut :

1. Akses menu **Pengaturan > Penilaian > Predikat Nilai Matakuliah**. 

![png](/img/screenshoot-penilaian/predikat/predikat-nilai-matakuliah.png)

2. Klik **Ubah**.

![png](/img/screenshoot-penilaian/predikat/mengubah-predikat-nilai-matakuliah.png)

3. Isi **Form** pada halaman tersebut.

![png](/img/screenshoot-penilaian/predikat/isi-form.png)

4. Jika telah selesai mengubah, klik **Simpan**.