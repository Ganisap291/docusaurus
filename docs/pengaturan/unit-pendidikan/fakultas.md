---
sidebar position: 2
---

# Fakultas

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Fakultas** ini berguna bagi kita dalam menambah, mengubah, menghapus, mencari dan melihat detail dari data Fakultas, berikut cara menggunakannya:

1. Menambah Data Fakultas.
2. Mengubah Data Fakultas.
3. Menghapus Data Fakultas.
4. Mencari Data Fakultas.
5. Melihat detail Data Fakultas.

# Menambah Data Fakultas

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Unit Pendidikan > Fakultas**.

![png](/img/screenshoot-unit-pendidikan/fakultas/fakultas.png)

2. Klik **Tambah** pada bagian kanan atas.

![png](/img/screenshoot-unit-pendidikan/fakultas/menambah-fakultas.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshoot-unit-pendidikan/fakultas/isi-form-tambah.png)

4. Jika telah selesai, klik **Simpan**.

# Mengubah Data Fakultas 

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Unit Pendidikan > Fakultas**.

![png](/img/screenshoot-unit-pendidikan/fakultas/fakultas.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

![png](/img/screenshoot-unit-pendidikan/fakultas/mengedit-fakultas.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshoot-unit-pendidikan/fakultas/isi-form-edit.png)

4. Jika telah selesai, klik **Simpan**.

# Menghapus Data Fakultas

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Unit Pendidikan > Fakultas**.

![png](/img/screenshoot-unit-pendidikan/fakultas/fakultas.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

![png](/img/screenshoot-unit-pendidikan/fakultas/isi-form-edit.png)

3. Pilih **Ya Hapus**.

# Mencari Data Fakultas

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Unit Pendidikan > Fakultas**.

![png](/img/screenshoot-unit-pendidikan/fakultas/fakultas.png)

2. Ketikan **KODE FAKULTAS** pada search bar yang terdapat pada kanan atas.

![png](/img/screenshoot-unit-pendidikan/fakultas/mencari-fakultas.png)

# Melihat Detail Data Fakultas

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Unit Pendidikan > Fakultas**.

![png](/img/screenshoot-unit-pendidikan/fakultas/fakultas.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

![png](/img/screenshoot-unit-pendidikan/fakultas/melihat-detail-fakultas.png)

3. Maka **Detail Data Fakultas** akan terlihat.