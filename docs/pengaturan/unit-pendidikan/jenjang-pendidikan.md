---
sidebar position: 1
---

# Jenjang Pendidikan

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Jenjang Pendidikan** ini berguna bagi kita dalam mengubah data Jenjang Pendidikan, berikut cara menggunakannya:

1. Mencari Data Jenjang Pendidikan.

# Mencari Data Jenjang Pendidikan

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Unit Pendidikan > Jenjang Pendidikan**.
![png](/img/screenshoot-unit-pendidikan/jenjang-pendidikan/jenjang-pendidikan.png)

2. Ketikan **JENJANG PENDIDIKAN** pada search bar yang terdapat pada kanan atas.
![png](/img/screenshoot-unit-pendidikan/jenjang-pendidikan/cari-jenjang.png)