---
sidebar position: 3
---

# Prodi

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Prodi** ini berguna bagi kita dalam mengubah data Prodi, berikut cara menggunakannya:

1. Menambah Data Program Studi.
2. Mengubah Data Program Studi.
3. Menghapus Data Program Studi.
4. Mencari Data Program Studi.

# Menambah Data Program Studi

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Unit Pendidikan > Program Studi**.

![png](/img/screenshoot-unit-pendidikan/prodi/prodi.png)

2. Klik **Tambah** pada bagian kanan atas.

![png](/img/screenshoot-unit-pendidikan/prodi/menambah-prodi.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshoot-unit-pendidikan/prodi/isi-form-tambah.png)

4. Jika telah selesai, klik **Simpan**.

# Mengubah Data Program Studi 

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Unit Pendidikan > Program Studi**.

![png](/img/screenshoot-unit-pendidikan/prodi/prodi.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

![png](/img/screenshoot-unit-pendidikan/prodi/edit-prodi.png)

3. Isi **Form** yang ada pada halaman tersebut.

![png](/img/screenshoot-unit-pendidikan/prodi/isi-form-edit.png)

4. Jika telah selesai, klik **Simpan**.

# Menghapus Data Program Studi

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Unit Pendidikan > Program Studi**.

![png](/img/screenshoot-unit-pendidikan/prodi/prodi.png)

2. Klik icon yang ditunjukan pada gambar tersebut.

![png](/img/screenshoot-unit-pendidikan/prodi/menghapus-prodi.png)

3. Pilih **Ya Hapus**.

# Mencari Data Program Studi

Adapun caranya sebagai berikut:

1. Akses menu **Pengaturan > Unit Pendidikan > Program Studi**.

![png](/img/screenshoot-unit-pendidikan/prodi/prodi.png)

2. Ketikan **Program Studi** pada search bar yang terdapat pada kanan atas.

![png](/img/screenshoot-unit-pendidikan/prodi/mencari-prodi.png)