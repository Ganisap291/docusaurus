---
sidebar position: 2
---


# Daftar Akun

:::info

Pendaftaran ini hanya dilakukan di website **pmb.unisma.ac.id**

:::


# Mendaftar Akun Menggunakan Email dan No Telpon  

1. Klik **Daftar** pada bagian kanan atas.

![png](/img/screenshoot-menu-pmb/daftar-akun/daftar.png)

2. Isi **form** yang ada pada halaman tersebut.

![png](/img/screenshoot-menu-pmb/daftar-akun/daftar-email-no-telp.png)

3. Jika telah selesai klik **Register**.

4. Maka akan muncul tampilan seperti ini jika akun berhasil di daftarkan.


![png](/img/screenshoot-menu-pmb/daftar-akun/tampilan-berhasil.png)


5. Klik kalimat yang ditunjukkan dalam gambar untuk kembali ke halaman login.

![png](/img/screenshoot-menu-pmb/daftar-akun/kembali-ke-halaman-login.png)


# Mendaftar Menggunakan Akun Google

1. Klik **Daftar** pada bagian kanan atas.

![png](/img/screenshoot-menu-pmb/daftar-akun/daftar.png)

2. Klik **Register with Google**.

![png](/img/screenshoot-menu-pmb/daftar-akun/daftar-akun-google.png)

3. Pilih akun google yang ingin di daftarkan.

4. Maka akan muncul tampilan seperti ini jika akun berhasil di daftarkan.


![png](/img/screenshoot-menu-pmb/daftar-akun/tampilan-berhasil.png)


5. Klik kalimat yang ditunjukkan dalam gambar untuk kembali ke halaman login.

![png](/img/screenshoot-menu-pmb/daftar-akun/kembali-ke-halaman-login.png)
