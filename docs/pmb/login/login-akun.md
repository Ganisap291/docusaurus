---
sidebar position: 2
---

# Login Akun    

:::info

Login ini hanya dilakukan di website **pmb.unisma.ac.id**

:::


# Login Menggunakan Email dan No Telpon  

1. Klik **Login** pada bagian kanan atas.

![png](/img/screenshoot-menu-pmb/login-akun/login.png)

2. Isi **email/no telp** yang ada pada halaman tersebut.

![png](/img/screenshoot-menu-pmb/login-akun/login-email-no-telp.png)

3. Jika telah selesai klik **Login**.

# Login Menggunakan Akun Google

1. Klik **Login** pada bagian kanan atas.

![png](/img/screenshoot-menu-pmb/login-akun/login.png)

2. Klik **Login with google account**.

![png](/img/screenshoot-menu-pmb/login-akun/login-google.png)

3. Pilih akun google yang akan di pakai untuk login.