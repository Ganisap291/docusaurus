---
sidebar position: 6
---

# Dosen Wali

# Paket Kelas B

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Pengguna **Dosen Wali** ini berguna bagi kita dalam mencari dan melihat detail dari data Dosen, berikut cara menggunakannya:


1. Melihat Detail .
2. Mencari Data Dosen.

# Melihat Detail

1. Akses menu **Perkuliahan > Dosen Wali**.

    [png](/img/screenshot-menu-perkuliahan/dosen-wali/melihat-detail/awalan-masuk.png)

2. Klik **>> Mahasiswa** yang terdapat pada tabel Aksi.

    ![png](/img/screenshot-menu-perkuliahan/dosen-wali/melihat-detail/lihat-detail.png)


# Mencari Data Dosen

1. Akses menu **Perkuliahan > Dosen Wali**.

    [png](/img/screenshot-menu-perkuliahan/dosen-wali/mencari-data/awalan-masuk.png)

2. Klik menu **Mahasiswa** yang terdapat pada tabel aksi.

    [png](/img/screenshot-menu-perkuliahan/dosen-wali/mencari-data/cari.png)
