---
sidebar_position: 1
---

# Pendahuluan

Menu **Perkuliahan** ini berfungsi untuk membantu BAAK, Kaprodi dan Admin Prodi dalam mengelola kegiatan perkuliahan.

Dalam panduan ini kita akan membahas fungsi dari menu **Perkuliahan** diantaranya :

1. Mengelola Dosen Wali
2. Mengelola Jadwal Perkuliahan
3. Mengelola Kelas
4. Mengelola Mahasiswa
5. Mengelola MBKM


:::note

Jika ada kekeliruan dalam dokumentasi ini mohon segera informasikan kepada kami tim SIESTA.

:::
