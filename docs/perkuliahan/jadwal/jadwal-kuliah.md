---
sidebar position: 1
---

# Jadwal Kuliah

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Jadwal Kuliah** ini berguna bagi kita dalam menambah, mengambil, dan mencari data Jadwal Kuliah berikut cara menggunakannya:

1. Menambah Data Jadwal Kuliah.
2. Mencari Data Jadwal Kuliah.
3. Mencetak Data Jadwal Kuliah Baru.
4. Mendownload Data Jadwal Kuliah Baru.
5. Mengubah Data Jadwal Kuliah. 
6. Melihat Detail Jadwal Kuliah
7. Mendownload Data Jadwal Kuliah.
8. Menghapus Data Jadwal Kuliah.
9. Membuat Jadwal Kuliah.

# Menambah Data Jadwal Kuliah

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal Kuliah**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/menambah-data/awalan-masuk.png)

2. Klik **Tambah** pada bagian kanan atas.  

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/menambah-data/tambah.png)

3. Isi **Form** yang terdapat pada halaman tersebut.
4. Jika telah selesai, klik **Simpan**.

# Mencari Data Jadwal Kuliah 

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal Kuliah**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mencari-data/awalan-masuk.png)

2. Klik **Search Bar** yang ada pada bagian kanan atas.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mencari-data/cari.png)

3. Ketikan **Data** yang ingin dicari pada Search Bar tersebut.


# Mencetak Data Jadwal Kuliah Baru

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal Kuliah**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mencetak-jadwal-baru/awalan-masuk.png)

2. Klik **Ambil Data** yang ada pada bagian kanan atas.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mencetak-jadwal-baru/ambil-data.png)

3. Isi **Form** yang ada pada halaman tersebut.
4. Jika sudah selesai, klik **Cetak**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mencetak-jadwal-baru/cetak.png)


# Mendownload Data Jadwal Kuliah Baru

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal Kuliah**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mendownload-jadwal-baru/awalan-masuk.png)

2. Klik **Ambil Data** yang ada pada bagian kanan atas.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mendownload-jadwal-baru/ambil-data.png)

3. Isi **Form** yang ada pada halaman tersebut.
4. Jika sudah selesai, klik **Xlxs**.

![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mendownload-jadwal-baru/download.png)


# Mengubah Data Jadwal Kuliah

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal Kuliah**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mengubah-data/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mengubah-data/ubah.png)

3. Isi **Form** yang ada pada halaman tersebut.
4. Jika sudah selesai, klik **Simpan**.

# Melihat Detail Jadwal Kuliah

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal Kuliah**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mendownload-jadwal/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mendownload-jadwal/lihat.png)


# Mendownload Data Jadwal Kuliah

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal Kuliah**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mendownload-jadwal/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mendownload-jadwal/lihat.png)

3. Klik **Download** pada detail data Jadwal Kuliah.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/mendownload-jadwal/download-data.png)


# Menghapus Data Jadwal Kuliah

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal Kuliah**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/menghapus-data/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/menghapus-data/hapus.png)

3. Klik **Ya Hapus**.

# Membuat Jadwal Kuliah.

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal Kuliah**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/membuat-jadwal/awalan-masuk.png)

2. Klik **Buat Jadwal Ujian** pada tabel aksi.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/membuat-jadwal/buat.png)

3. Pilih **UTS** atau **UAS**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-kuliah/membuat-jadwal/pilih.png)

4. Isi **Form** yang ada pada halaman terebut.
5. JIka sudah selesai, klik **Simpan**.