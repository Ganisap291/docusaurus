---
sidebar position: 2
---

# Jadwal Kuliah

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Validasi Jadwal Kuliah** ini berguna bagi kita dalam menutup, dan mencari data Jadwal Kuliah berikut cara menggunakannya:

1. Menutup Data Jadwal Kuliah.
2. Mencari Data Jadwal Kuliah.


# Menutup Data Jadwal Kuliah

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Validasi Jadwal Kuliah**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/validasi-jadwal-kuliah/menutup-data/awalan-masuk.png)

2. **Mencetang Kotak** pada bagian yang berada di sebelah data kelas.  

    ![png](/img/screenshot-menu-perkuliahan/jadwal/validasi-jadwal-kuliah/menutup-data/centang.png)

3. Klik **Tutup Kelas** pada bagian kiri atas.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/validasi-jadwal-kuliah/menutup-data/tutup.png)

4. Jika telah selesai, klik **Tutup Kelas**.

# Mencari Data Jadwal Kuliah

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Validasi Jadwal Kuliah**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/validasi-jadwal-kuliah/mencari-data/awalan-masuk.png)

2. Ketikan **Kelas, Fakultas - Prodi** pada search bar yang terdapat pada bagian kanan atas. 

    ![png](/img/screenshot-menu-perkuliahan/jadwal/validasi-jadwal-kuliah/mencari-data/cari.png)