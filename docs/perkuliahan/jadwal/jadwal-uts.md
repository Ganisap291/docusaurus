---
sidebar position: 3
---

# Jadwal UTS

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Jadwal UTS** ini berguna bagi kita dalam mencari data Jadwal UTS berikut cara menggunakannya:

1. Mencari Data Jadwal UTS.
2. Ubah Data Jadwal UTS.
3. Menghapus Data Jadwal UTS.
4. Mencetak Presensi.
5. Mencetak Berita Acara.


## Mencari Data Jadwal UTS

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal UTS**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-uts/mencari-data/awalan-masuk.png)

2. Klik **Search Bar** yang terdapat pada bagian kanan atas.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-uts/mencari-data/cari.png)

3. Ketikan **Data** yang ingin dicari pada search bar tersebut.

## Ubah Data Jadwal UTS

Adapun caranya sebagai berikut:

1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal UTS**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-uts/mengubah-data/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-uts/mengubah-data/ubah.png)

3. Isi **Form** yang ada pada halaman tersebut.
4. Klik **Simpan**.

## Menghapus Data Jadwal UTS

Adapun caranya sebagai berikut:


1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal UTS**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-uts/menghapus-data/awalan-masuk.png)

2. Klik icon yang ditunjukan pada gambar.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-uts/menghapus-data/hapus.png)

3. Klik **Ya Hapus**.

## Mencetak Presensi

Adapun caranya sebagai berikut:


1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal UTS**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-uts/mencetak-presensi/awalan-masuk.png)

2. Klik **Prensensi** yang terdapat pada menu **Cetak**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-uts/mencetak-presensi/presensi.png)

3. Cetak Prensensi.

## Mencetak Berita Acara

Adapun caranya sebagai berikut:


1. Akses menu **Perkuliahan** > **Jadwal** > **Jadwal UTS**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-uts/mencetak-berita-acara/awalan-masuk.png)

2. Klik **Berita Acara** yang terdapat pada menu **Cetak**.

    ![png](/img/screenshot-menu-perkuliahan/jadwal/jadwal-uts/mencetak-berita-acara/berita-acara.png)
 
3. Cetak Berita Acara.