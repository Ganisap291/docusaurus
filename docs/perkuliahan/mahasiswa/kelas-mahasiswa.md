---
sidebar position: 4
---

# Kelas Mahasiswa

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu **Kelas Mahasiswa** ini berguna bagi kita dalam mencari, dan melihat detail data Kelas Mahasiswa, berikut cara menggunakannya:

1. Mencari Data Kelas Mahasiswa.
2. Melihat Detail Data Kelas Mahasiswa.

# Mencari Data Kelas Mahasiswa

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa> Kelas Mahasiswa**.

    ![png](/img/screenshot-menu-perkuliahan/mahasiswa/kelas-mahasiswa/mencari-data/awalan-masuk.png)

2. Ketikan **NAMA KELAS / FAKULTAS/PRODI** pada search bar yang terdapat pada kanan atas.

    ![png](/img/screenshot-menu-perkuliahan/mahasiswa/kelas-mahasiswa/mencari-data/cari.png)

# Melihat Detail Data Kelas Mahasiswa

Adapun caranya sebagai berikut:

1. Akses menu **Mahasiswa> Kelas Mahasiswa**.

    ![png](/img/screenshot-menu-perkuliahan/mahasiswa/kelas-mahasiswa/melihat-detail/awalan-masuk.png)

2. Klik **>> Mahasiswa** pada tabel Pemetaan.

    ![png](/img/screenshot-menu-perkuliahan/mahasiswa/kelas-mahasiswa/melihat-detail/lihat-detail.png)
