---
sidebar position: 3
---

# Kelas

:::info
Pembuatan Kegiatan ini hanya dilakukan Web Aplikasi SIAKAD Unisma
:::

**Persyaratan**

1. Login Menggunakan Akun BAAK di url https://siakad.unisma.ac.id

**Pembahasan**

Menu Periode Kelas ini berguna bagi kita dalam menambah, mengubah, dan mancari data Kelas, berikut cara menggunakannya:


1. Menambah Data Kelas.
2. Mengubah Data Kelas.
3. Menghapus Data Kelas.
4. Mencari Data Kelas.

# Menambah Data Kelas

1. Akses menu **Perkuliahan > Kelas**.
2. Klik **Tambah** pada bagian kanan atas.
3. Isi **Form** pada halaman tersebut.
4. Serta tak lupa memilih status pada halaman tersebut.
5. Jika telah selesai, klik **Simpan**.

# Mengubah Data Kelas

1. Akses menu **Perkuliahan > Kelas**.
2. Klik icon berikut:
3. Isi **Form** pada halaman tersebut.
4. Serta tak lupa memilih status pada halaman tersebut.
5. Jika telah selesai, klik **Simpan**.

# Menghapus Data Kelas

1. Akses menu **Perkuliahan > Kelas**.
2. Klik icon berikut:
3. Pilih **Ya Hapus**.

# Mencari Data Jenis Kelas

1. Akses menu **Pengaturan > Yudisium > Periode Yudisium**.
2. Ketikan **NAMA KELAS** pada search bar yang terdapat pada kanan atas.